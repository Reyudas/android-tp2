package com.example.tp2;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class WineAddActivity extends AppCompatActivity {

    String name;
    Wine wine;
    EditText wineName;
    EditText wineRegion;
    EditText wineLoc;
    EditText wineClimate;
    EditText wineArea;
    WineDbHelper wineDB = new WineDbHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        Intent intent = getIntent();
        final Wine wine = (Wine) intent.getParcelableExtra("wine");
        this.wineName=findViewById(R.id.wineName);

        this.wineRegion=findViewById(R.id.editWineRegion);

        this.wineLoc=findViewById(R.id.editLoc);

        this.wineClimate=findViewById(R.id.editClimate);

        this.wineArea=findViewById(R.id.editPlantedArea);

        final Button button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (wineName.getText().toString().isEmpty()==false){
                    Wine wineTemp = new Wine(wineName.getText().toString(),
                            wineRegion.getText().toString(),
                            wineLoc.getText().toString(),
                            wineClimate.getText().toString(),
                            wineArea.getText().toString());

                    if (wineDB.addWine(wineTemp) == true){
                        //Log.d("Update","Success");
                        //Intent intent = new Intent(WineAddActivity.this, MainActivity.class);
                        finish();
                        //startActivity(intent);
                    }
                    else {
                        WineAddActivity.displayDialog("Ajout impossible",
                                "Le nom du vin est déja utilisé",
                                WineAddActivity.this);
                    }


                }
                else{
                    WineAddActivity.displayDialog("Ajout impossible",
                            "Le nom du vin dois être non vide",
                            WineAddActivity.this);

                }

            }
        });
    }

    public static void displayDialog(String title, String message, Context context){

        final AlertDialog alert = new AlertDialog.Builder(context).create();
        alert.setTitle(title);
        alert.setMessage(message);
        alert.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alert.dismiss();
            }
        }, 4000);
    }

}
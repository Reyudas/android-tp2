package com.example.tp2;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class WineActivity extends AppCompatActivity {

    String name;
    Wine wine;
    EditText wineName;
    EditText wineRegion;
    EditText wineLoc;
    EditText wineClimate;
    EditText wineArea;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        WineDbHelper wineDB = new WineDbHelper(this);

        Intent intent = getIntent();
        final Wine wine = (Wine) intent.getParcelableExtra("wine");
        this.wineName=findViewById(R.id.wineName);
        this.wineName.setText(wine.getTitle());

        Log.d("WineActivity","id : "+wine.getId());

        this.wineRegion=findViewById(R.id.editWineRegion);
        this.wineRegion.setText(wine.getRegion());

        this.wineLoc=findViewById(R.id.editLoc);
        this.wineLoc.setText(wine.getLocalization());

        this.wineClimate=findViewById(R.id.editClimate);
        this.wineClimate.setText(wine.getClimate());

        this.wineArea=findViewById(R.id.editPlantedArea);
        this.wineArea.setText(wine.getPlantedArea());

        final Button button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WineDbHelper wineDB = new WineDbHelper(WineActivity.this);
                if (wineName.getText().toString().isEmpty()==false){

                    Log.d("WineActivity","id : "+wine.getId());
                    Wine wineTemp = new Wine(wine.getId(),wineName.getText().toString(),
                            wineRegion.getText().toString(),
                            wineLoc.getText().toString(),
                            wineClimate.getText().toString(),
                            wineArea.getText().toString());

                            wineDB.updateWine(wineTemp);
                            //Log.d("Update","Success");
                            //Intent intent = new Intent(WineActivity.this, MainActivity.class);

                            //startActivity(intent);
                            finish();
                }
                else{
                    WineActivity.displayDialog("Modification impossible",
                            "Le nom du vin dois être non vide",
                            WineActivity.this);

                }

            }
        });
    }

    public static void displayDialog(String title, String message, Context context){

        final AlertDialog alert = new AlertDialog.Builder(context).create();
        alert.setTitle(title);
        alert.setMessage(message);
        alert.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alert.dismiss();
            }
        }, 4000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
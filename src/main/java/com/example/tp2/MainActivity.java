package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.database.Cursor;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    WineDbHelper WineDB = new WineDbHelper(this);
    Cursor c ;
    ArrayList<String> wineL = new ArrayList<String>();

    ArrayList<String> test = new ArrayList<String>(Arrays.asList("Test","Test 1","Test 2"));

    ArrayList<Wine> wineArray = new ArrayList<Wine>();

    Cursor cur = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //WineDB.populate();

        c=WineDB.fetchAllWines();

        c.moveToFirst();

        while(!c.isAfterLast()) {


            Wine wineTemp =WineDB.cursorToWine(c);

            Log.d("Main","id cursor: "+wineTemp.getId());

            wineL.add(wineTemp.getTitle());
            wineArray.add(wineTemp);
            c.moveToNext();
        }

        //Log.d("test","vin:"+wineL);
        //Log.d("test","vin:"+wineArray);

        listView = (ListView) findViewById(R.id.listView);

        registerForContextMenu(listView);

        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                c,
                new String[] {WineDbHelper.COLUMN_NAME,WineDbHelper.COLUMN_WINE_REGION},
                new int[] { android.R.id.text1,android.R.id.text2});

        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Wine wine = wineArray.get(position);
                //Log.d("MainActivity","id : "+wine.getId());
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine",wine);
                startActivity(intent);
                //finish();

            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, WineAddActivity.class);
                startActivity(intent);
                //finish();
            }
        });

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (item.getItemId()==R.id.delete){
            Wine temp =  wineArray.get(info.position);
            Cursor cTemp = c;
            cTemp.moveToPosition(info.position);
            WineDB.deleteWine(cTemp);
            recreate();
            return super.onContextItemSelected(item);
        }
        else {
            return super.onContextItemSelected(item);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu_main; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.delete) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRestart(){
        super.onRestart();

        final WineDbHelper Winetemp = new WineDbHelper(this);

        final ArrayList<Wine> wineLTEMP = new ArrayList<Wine>();
        cur=Winetemp.fetchAllWines();

        wineL=new ArrayList<String>();

        /*cur.moveToFirst();

        while(!cur.isAfterLast()) {


            Wine wineTemp =WineDB.cursorToWine(c);

            Log.d("Main","id cursor: "+wineTemp.getId());

            wineL.add(wineTemp.getTitle());
            wineLTEMP.add(wineTemp);
            cur.moveToNext();
        }*/

        listView = (ListView) findViewById(R.id.listView);

        registerForContextMenu(listView);

        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cur,
                new String[] {WineDbHelper.COLUMN_NAME,WineDbHelper.COLUMN_WINE_REGION},
                new int[] { android.R.id.text1,android.R.id.text2});

        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                final Cursor c =(Cursor) parent.getItemAtPosition(position);
                Wine wine = Winetemp.cursorToWine(c);
                //Log.d("MainActivity","id : "+wine.getId());
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine",wine);
                startActivity(intent);
                //finish();

            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, WineAddActivity.class);
                startActivity(intent);
                //finish();
            }
        });


        //this.recreate();

    }
}
